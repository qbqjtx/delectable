package service;

import entity.Date;
import entity.Order;
import entity.Report;
import main.Delectable;
import main.Method;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;

/**
 * Created by adrianoob on 4/27/16.
 */
@Path("report")
public class ReportService {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getReportList() {
        ArrayList<String> reports = new ArrayList<>();
        reports.add(Method.arrayToString(new String[]{"id", 801+"", "name", "\"order to deliver today\""}));
        reports.add(Method.arrayToString(new String[]{"id", 802+"", "name", "\"order to deliver tomorrow\""}));
        reports.add(Method.arrayToString(new String[]{"id", 803+"", "name", "\"order to deliver between start_date and end_date\""}));
        reports.add(Method.arrayToString(new String[]{"id", 804+"", "name", "\"revenue report between start_date and end_date\""}));

        return Response.ok(reports+"").build();
    }


    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response generateReport(@PathParam("id") String id, @QueryParam("start_date") String d1, @QueryParam("end_date") String d2) {
        Response res;
        if (id.equals("801"))
            res = getOrdersToDeliverToday();
        else if (id.equals("802"))
            res = getOrdersToDeliverTomorrow();
        else if (id.equals("803"))
            res = getOpenOrderBetweenDates(d1, d2);
        else if (id.equals("804"))
            res = revenueReport(d1, d2);
        else {
            res = Response.status(404).entity(Method.notFound(id + " report not found")).type(MediaType.TEXT_HTML).build();
        }
        return res;
    }

    public Response getOrdersToDeliverToday() {
        Date today = new Date();
        ArrayList<Order> olist = Delectable.ordersToDeliverOnDate(today);
        Report report = new Report(801, "orders to deliver today", olist.toArray(new Order[olist.size()]));
        return Response.ok(report).build();
    }

    public Response getOrdersToDeliverTomorrow() {
        Date today = new Date();
        Date tomorrow = today.roll(1);
        ArrayList<Order> olist = Delectable.ordersToDeliverOnDate(tomorrow);
        Report report = new Report(802, "orders to deliver tomorrow", olist.toArray(new Order[olist.size()]));
        return Response.ok(report).build();
    }

    public Response getOpenOrderBetweenDates(String ds1, String ds2) {
        Date start = ds1 == null ? null : new Date(ds1);
        Date end   = ds2 == null ? null : new Date(ds2);
        ArrayList<Order> all = Delectable.allOrdersInTheTimeInterval(start, end);
        ArrayList<Order> deliveredList = new ArrayList<>();
        for (int i = 0; i < all.size(); i++) {
            Order  ord = all.get(i);
            if (ord.getStatus().equals(Order.OPEN))
                deliveredList.add(ord);
        }
        String name = "orders to deliver ";
        if (ds1 == null)
            name = name + "before " + ds2;
        else if (ds2 == null)
            name = name + "after " + ds1;
        else
            name = name + "between " + ds1 + " and " + ds2;
        Order [] list = deliveredList.toArray(new Order[deliveredList.size()]);
        Report report = new Report(803, name, list);
        return Response.ok(report).build();
    }

    public Response revenueReport(String d1, String d2) {
        Date start = d1 == null ? null : new Date(d1);
        Date end   = d2 == null ? null : new Date(d2);
        ArrayList<Order> all = Delectable.allOrdersInTheTimeInterval(start, end);
        int totalOrder = 0;
        int cancelledOrder = 0;
        int openOrder = 0;
        int deliveredOrder = 0;
        double revenue = 0;
        double surchargeRevenue = 0;

        for (int i = 0; i < all.size(); i++) {
            Order ord = all.get(i);
            totalOrder++;
            if (ord.getStatus().equals(Order.CANCELLED))
                cancelledOrder++;
            else {
                revenue += ord.totalPriceWithOutSurcharge();
                surchargeRevenue += ord.getSurcharge();
                if (ord.getStatus().equals(Order.OPEN))
                    openOrder++;
                else
                    deliveredOrder++;
            }

        }

        String json = Method.arrayToString(new String[]{
                "id", 803+"",
                "name", "\"revenue report\"",
                "start_date", d1 == null ? "\"\"" : "\""+d1+"\"",
                "end_date",   d2 == null ? "\"\"" : "\""+d2+"\"",
                "order_placed", totalOrder+"",
                "order_cancelled", cancelledOrder+"",
                "order_delivered", deliveredOrder+"",
                "order_open", openOrder+"",
                "food_revenue", String.format("%.2f", revenue),
                "surcharge_revenue", String.format("%,2f", surchargeRevenue),
                "note", "\"revenue and surcharge_revenue are calculated from open and delivered orders\""
        });
        return Response.ok(json).build();
    }
}
