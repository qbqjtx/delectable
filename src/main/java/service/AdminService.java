package service;

import entity.Item;
import main.Delectable;
import main.Method;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;

/**
 * Created by adrianoob on 4/25/16.
 */

@Path("admin")
public class AdminService {

    @GET
    @Path("holiday")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getHolidayList() {
        return Response.ok(Delectable.getHolidaylist()).build();
    }

    @PUT
    @Path("holiday")
    @Produces(MediaType.APPLICATION_JSON)
    public Response addHoliday(@QueryParam("description") String des, @QueryParam("rule") String rul) {
        boolean success = Delectable.addHoliday(des, rul);
        Response res;
        if (success)
            res = Response.created(URI.create("http://localhost:8080/delectable/admin/holiday")).build();
        else
            res = Response.status(400).entity(Method.badRequest("rule is malformated")).type(MediaType.APPLICATION_JSON).build();
        return res;
    }

    @PUT
    @Path("menu/category/define")
    @Produces(MediaType.APPLICATION_JSON)
    public Response defineNewCategory(@QueryParam("new_category") String category) {
        String feedback = Delectable.defineNewMenuCategory(category);
        Response res;
        URI uri = URI.create("http://localhost:8080/delectable/menu/all_category");
        if (feedback.contains(" got added as a category")) {
            res = Response.created(uri).build();
        } else {
            res = Response.status(400).header("Location", uri).entity(Method.badRequest(feedback)).type(MediaType.TEXT_HTML).build();
        }
        return res;
    }


    @PUT
    @Path("menu")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addItemToMenu(Item f) {
        Delectable.addItemToMenu(f);
        int id = f.getId();
        String json = String.format("{ \"id\" : %d }", id);
        URI uri = URI.create(String.format("http://localhost:8080/delectable/menu/%d",id));
        return Response.created(uri).entity(json).build();
    }

    @POST
    @Path("menu/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response changePriceOfItemWIthId(Item f, @PathParam("id") int id) {
        int code, newID = -1;
        String feedback = "";
        if (f.getId() != id) {
            code = 400; // link differ from json data
            feedback = "link is different from json data";
        } else if (f.getId() == -1) {
            code = 400; // item id is missing from json
            feedback = "id is missing from json data";
        } else if (f.getPrice() == -1) {
            code = 400; // item price is missing from json
            feedback = "price is missing from json data";
        } else {
            code = 204;
            newID = Delectable.changeItemPrice(id, f.getPrice());
        }

        Response res;
        if (code == 400) {
            URI uri = URI.create(String.format("http://localhost:8080/delectable/menu/%d", id));
            res = Response.status(400).header("Location", uri).entity(Method.badRequest(feedback)).type(MediaType.TEXT_HTML).build();
        } else {
            URI uri = URI.create(String.format("http://localhost:8080/delectable/menu/%d", newID));
            res = Response.status(204).header("Location", uri).build();
        }
        return res;
    }

    @GET
    @Path("surcharge")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSurcharge() {
        return Response.ok(String.format(" { \"surcharge\" : %.2f } ", Delectable.getSurcharge())).build();
    }


    @POST
    @Path("surcharge")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response setSurcharge(String input) {
        String str = input.replace("\n", "");
        str = str.replace("{", "");
        str = str.replace("}", "");
        str = str.replace("[", "");
        str = str.replace("]", "");
        String [] segs = str.split(",");
        double surcharge = -1;
        for (String seg : segs) {
            String temp = seg.replace(" ", "");
            temp = temp.replace("\"", "");
            String [] list = temp.split(":");
            if (list.length == 2) {
                String key = list[0];
                String value = list[1];
                if (key.contains("surcharge")) {
                    try {
                        surcharge = Double.parseDouble(value);
                        break;
                    } catch (NumberFormatException nfe) {}
                }
            }
        }
        URI uri = URI.create("http://localhost:8080/delectable/admin/surcharge");
        Response res;
        if (surcharge < 0) {
            res = Response.status(400).header("Location", uri).entity(Method.badRequest("data is missing or malformed")).type(MediaType.TEXT_HTML).build();
        } else {
            res = Response.status(204).header("Location", uri).build();
            Delectable.setSurcharge(surcharge);
        }
        return res;
    }
}
