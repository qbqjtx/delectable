package service;

import entity.Date;
import entity.Item;
import entity.Order;
import main.Delectable;
import main.Method;
import struct.OrderDetail;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.ArrayList;

/**
 * Created by adrianoob on 4/26/16.
 */
@Path("order")
public class OrderService {

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getOrderWithId(@PathParam("id") int id) {
        Order ord =  Delectable.getOrderById(id);
        Response res;
        if (ord.getId() == -1)
            res = Response.status(404).type(MediaType.TEXT_HTML).entity(Method.notFound(String.format("order with id of %d not found", id))).build();
        else
            res = Response.status(200).entity(ord).build();
        return res;
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response placeOrder(Order ord) {
        boolean goodOrder = true;
        String msg = "";
        for (OrderDetail od: ord.getOrder_detail()) {
            Item f = Delectable.getItemWithId(od.getId());
            if (f.getId() == -1) {
                goodOrder = false;
                msg = "food item with id of " + od.getId() + " is not on menu";
                break;
            } else {
                if (od.getCount() < f.getMinimum()) {
                    // food is good, but order amount is below minimum
                    goodOrder = false;
                    msg = "food item with id of " + od.getId() + " requires a minimum order of " + f.getMinimum() + " in quantity";
                    break;
                }
            }
        }

        Response res;
        if (goodOrder) {
            String id = Delectable.submitOrder(ord) + "";
            String [] s = {"id", id, "cancel_url", "\"order/cancel/" + id + "\""};
            URI uri = URI.create(String.format("http://localhost:8080/delectable/order/%s",id));
            res = Response.created(uri).entity(Method.arrayToString(s)).type(MediaType.APPLICATION_JSON).build();
        } else {
            res = Response.status(400).entity(Method.badRequest(msg)).type(MediaType.TEXT_HTML).build();
        }
        return res;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllOrderOrOrdersToDeliverOnDate(@QueryParam("date") String ds) {
        ArrayList<Order> list;
        if (ds == null)
            list = Delectable.getAllOrders();
        else
            list = Delectable.ordersToDeliverOnDate(new Date(ds));

        ArrayList<String> result = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            Order o = list.get(i);
            String [] summary =
                    {"id", o.getId()+"",
                            "order_date", "\""+o.getOrder_date()+"\"",
                            "delivery_date", "\""+o.getDelivery_date()+"\"",
                            "amount", String.format("%.2f",o.totalPriceWithOutSurcharge()),
                            "surcharge", String.format("%.2f",o.getSurcharge()),
                            "status", "\""+o.getStatus()+"\"",
                            "ordered_by", "\""+o.getCustomer().getEmail()+"\""};

            result.add(Method.arrayToString(summary));
        }
        return Response.ok(result+"").build();
    }

    @POST
    @Path("cancel/{oid}")
    @Produces(MediaType.TEXT_PLAIN)
    public Response cancelOrderById(@PathParam("oid") int oid) {
        Order o = Delectable.getOrderById(oid);
        Response res;
        if (o.getId() == -1) {
            res = Response.status(400).type(MediaType.TEXT_HTML).entity(Method.badRequest(String.format("order with id of %d not found", oid))).type("text/plain").build();
        } else {
            Date today = new Date();
            Date deliveryDate = new Date(o.getDelivery_date());
            if (deliveryDate.equals(today)) {
                res = Response.status(400).type(MediaType.TEXT_HTML).entity(Method.badRequest(String.format("order with id of %d will be delivered today and can't be cancelled", oid))).type("text/plain").build();
            } else {
                o.setStatus(Order.CANCELLED);
                res = Response.status(200).build();
            }
        }
        return res;
    }

}
