package service;

import entity.Item;
import main.Delectable;
import entity.Category;
import main.Method;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by adrianoob on 4/25/16.
 *
 * Menu:
 *   1. get all menu category
 *   2. get the entire menu
 *   3. get an item by id
 */
@Path("menu")
public class MenuService {

    @GET
    @Path("all_category")
    @Produces(MediaType.APPLICATION_JSON)
    public Category [] getAllMenuCategory() {
        return Delectable.getAllItemCategories();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Item[] getAllItemsFromMenu() {
        return Delectable.getAllTheItemsFromMenu();
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getItemWithId(@PathParam("id") int id) {
        Item f = Delectable.getItemWithId(id);
        Response res;
        if (f.getId() != -1) {
            res = Response.ok(f).build();
        } else {
            res = Response.status(404).entity(Method.notFound(String.format("item with id of %d not found", id))).type(MediaType.TEXT_HTML).build();
        }
        return res;
    }
}
