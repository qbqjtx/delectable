package service;

import entity.Customer;
import entity.Order;
import main.Delectable;
import main.Method;
import struct.Customer_Orders;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;

/**
 * Created by adrianoob on 4/27/16.
 */

@Path("customer")
public class CustomerService {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<Customer> getAllCustomer(@QueryParam("query_string") String key) {
        ArrayList<Customer> list;
        if (key == null)
            list = Delectable.getAllCustomers();
        else
            list = Delectable.searchCustomers(key);
        return list;
    }

    @GET
    @Path("{cid}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCustomerById(@PathParam("cid") int id) {
        Customer cus = Delectable.getCustomerById(id);
        Response res;
        if (cus.getId() == -1) {
            res = Response.status(404).type(MediaType.TEXT_HTML).entity(Method.notFound(String.format("customer with id of %d does not exist", id))).build();
        } else {
            ArrayList<Order> all = Delectable.getAllOrders();
            ArrayList<Order> list = new ArrayList<>();
            for (int i = 0; i < all.size(); i++) {
                if (all.get(i).getCustomer().equals(cus))
                    list.add(all.get(i));
            }
            Customer_Orders co = new Customer_Orders(cus, list.toArray(new Order[list.size()]));
            res = Response.ok(co).build();
        }
        return res;
    }


}
