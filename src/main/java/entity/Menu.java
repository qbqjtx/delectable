package entity;

import java.util.ArrayList;

/**
 * Created by adrianoob on 4/19/16.
 *
 * This class keeps track of a group Item objects, like a menu. It provides functionalities like
 * -- browse the entire menu
 * -- browse menu by one category
 */

public class Menu {
    private int totalItem = 0; // used to record index and create index for new item

    private double surcharge = 0;
    private final ArrayList<Item> itemList = new ArrayList();
    private final ArrayList<Item> deletedList = new ArrayList();

    public Menu(double surcharge) {
        this.surcharge = surcharge;
        new Category();
    }

    public Menu() {
        new Category();
    }

    /* ***************************************** *
     * MANAGER: add / delete / modify food items *
     * ***************************************** */
    /**
     * add one food item to the menu
     * @param food
     */
    public void addItemToMenu(Item food) {
        food.organizeCategory();
        itemList.add(food);
        food.setId(totalItem++);
        food.setHasHistory(true);
    }

    /**
     * Make the item look deleted by moving it to deleteList.
     * @param id - setId of food item in the menu as well as the index of the item in the ArrayList
     */
    public void deleteItemFromMenu(int id) {
        Item temp = itemList.remove(id);
        deletedList.add(temp);
        itemList.add(id, null);
    }

    /**
     * 1. move old item to deletedList
     * 2. clone the old item to make a new one, change the price
     * 3. add the new item to the end of itemList
     * 4. return new item's id
     * @param id
     * @param newPrice
     * @return
     */
    public int changeItemPrice(int id, double newPrice) {
        Item oldItem = getItemWithIdOnMenu(id);
        int feedbackID = -1;
        if (oldItem.getId() > -1) {
            itemList.remove(id);
            itemList.add(id, null);
            deletedList.add(oldItem);

            oldItem.setPrice(newPrice);
            Item newItem = oldItem.confirmChanges();
            addItemToMenu(newItem);
            feedbackID = newItem.getId();
        }
        return feedbackID;
    }

    /* ******************************** *
     * MANAGER: operation on categories *
     * ******************************** */
    /**
     * Define a new menu category (for food or drink)
     * @param cat - new category to create
     */
    public String defineNewCategory(String cat) {
        return Category.addPreDefinedCategory(cat);
    }
    /**
     * Get all the defined categories in an array. An empty array means no pre-defined categories.
     * @return - an array contains Categories
     */
    public Category [] definedCategory() {
        return Category.getCategoryPoolAsArray();
    }

    /* ************************************* *
     * CUSTOMER / MANAGER: browse menu items *
     * ************************************* */
    /**
     * get all the items in itemList as an array in the ID orders
     * @return - an array that contains all items from itemList
     */
    public Item [] getWholeMenu() {
        ArrayList<Item> temp = new ArrayList<>();
        for (int i = 0; i < itemList.size(); i++) {
            if (itemList.get(i) != null) {
                temp.add(itemList.get(i));
            }
        }
        int size = temp.size();
        Item [] r = new Item[size];
        for (int i = 0; i < size; i++) {
            r[i] = temp.get(i);
        }
        return r;
    }

    /**
     * get all items from certain category in itemList as an array
     * @param ca - category
     * @return   - an array that contains all items of the category
     */
    public Item [] getItemFromCategory(Category ca) {
        ArrayList<Item> temp = new ArrayList<>();
        for (int i = 0; i < itemList.size(); i++) {
            Item f = itemList.get(i);
            if (f != null && f.belongsToCategory(ca)) {
                temp.add(itemList.get(i));
            }
        }

        Item [] list = new Item[temp.size()];
        for (int i = 0; i < temp.size(); i++) {
            list[i] = temp.get(i);
        }
        return list;
    }


    /**
     * get all the items in deletedList
     * @return - an array that contains all items from deletedList
     */
    public Item [] getWholeDeletedMenu() {
        int size = deletedList.size();
        Item [] r = new Item[size];
        for (int i = 0; i < size; i++) {
            r[i] = deletedList.get(i);
        }
        return r;
    }

    /* ***************** *
     * getter and setter *
     * ***************** */
    public double getSurcharge() { return surcharge; }

    public void setSurcharge(double s) { this.surcharge = s; }

    /**
     * look up item with the setId both on and off the menu
     * @param id - setId of the item
     * @return - the item. null if not found
     */
    public Item getItemWithIdOnAndOffMenu(int id) {
        Item r = new BadItem();
        if (id >= 0 && id < itemList.size()) {
            Item temp = itemList.get(id);
            if (temp != null) {
                r = temp;
            } else {
                for (int i = 0; i < deletedList.size(); i++) {
                    Item d = deletedList.get(i);
                    if (d.getId() == id) {
                        r = d;
                        break;
                    }
                }
            }
        }
        return r;
    }

    /**
     * look up item with the setId on the menu
     *   - return BadItem if not on or off menu
     *   - return DeletedItem if exists off menu
     *   - return the item if it's on menu
     * @param id - setId of the item
     * @return - the item. null if not found
     */
    public Item getItemWithIdOnMenu(int id) {
        Item r = new BadItem();
        if (id >= 0 && id < itemList.size()) {
            // id exists on or off menu
            if (itemList.get(id) == null)
                r = new DeletedItem();
            else
                r = itemList.get(id);
        }
        return r;
    }

}
