package entity;

import java.util.Calendar;

/**
 * This class record date information and check if it's weekend or holiday
 * Created by adrianoob on 4/24/16.
 */
public class Date {
    private Calendar calendar;
    private int year;
    private int month;
    private int day;
    private int hour;
    private int minute;

    /**
     * Constructor that use current date and time
     */
    public Date() {
        calendar = Calendar.getInstance();
        year   = calendar.get(Calendar.YEAR);
        month  = calendar.get(Calendar.MONTH) + 1;
        day    = calendar.get(Calendar.DAY_OF_MONTH);
        hour   = calendar.get(Calendar.HOUR);
        minute = calendar.get(Calendar.MINUTE);
    }

    /**
     * Constructor that use input date and time
     * @param y  - year
     * @param m  - month
     * @param d  - day
     * @param h  - hour
     * @param mi - minute
     */
    public Date(int y, int m, int d, int h, int mi) {
        calendar = Calendar.getInstance();
        calendar.set(y, m-1, d, h, mi);
        year = y; month = m; day = d;
        hour = h; minute = mi;
    }

    /**
     * String format is "yyyymmdd"
     */
    public Date(String date) {
        calendar = Calendar.getInstance();
        year  = Integer.parseInt(date.substring(0,4));
        month = Integer.parseInt(date.substring(4,6));
        day   = Integer.parseInt(date.substring(6,8));
        hour  = calendar.get(Calendar.HOUR);
        minute = calendar.get(Calendar.MINUTE);
        calendar.set(year, month - 1, day, hour, minute);
    }

    /**
     * check if the date is at weekend
     * @return - boolean indicating whether it's at weekend
     */
    public boolean isWeekend() {
        int d = calendar.get(Calendar.DAY_OF_WEEK);
        return d == 7 || d == 1;
    }

    public String getHoliday() {
        new Holiday();
        return Holiday.dateIsHoliday(this);
    }

    /**
     * equals method that depends on date, not on time
     * @param d - date object to compare
     * @return  - boolean indicating equality
     */
    public boolean equals(Date d) {
        return year == d.year && month == d.month && day == d.day;
    }

    /**
     * roll forwards with positive input, roll backwards with negtive input
     * @param days - amount of days to roll / roll back
     * @return     - new date derived from rolling
     */
    public Date roll(int days) {
        Date nd = new Date(year, month, day, 0, 0);
        nd.calendar.add(Calendar.DATE, days);
        nd.year  = nd.calendar.get(Calendar.YEAR);
        nd.month = nd.calendar.get(Calendar.MONTH) + 1;
        nd.day   = nd.calendar.get(Calendar.DATE);
        return nd;
    }

    public boolean isAfter(Date d2) {
        return calendar.after(d2.calendar);
    }

    public boolean isBefore(Date d2) {
        return calendar.before(d2.calendar);
    }


    /* ******************************* *
     * *********** holiday *********** *
     * ******************************* */

    /* 1/1       new year
     * 1/3monday martin luther king day
     * 5/5monday memorial day
     * 7/4       independence day
     * 9/1monday labor day
     * 11/24     thanksgiving
     * 12/25     christmas
     */




    /* ************************************* *
     * ********* getter and setter ********* *
     * ************************************* */
    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public String toString() {
        return String.format("%04d%02d%02d", year, month, day);
    }

}
