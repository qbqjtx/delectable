package entity;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by adrianoob on 4/23/16.
 */
@XmlRootElement
public class Customer {
    private int id = -1;
    private String name;
    private String email;
    private String phone;

    public Customer() {}

    public Customer(String n, String e, String p) {
        name  = n;
        email = e;
        phone = p;
    }

    public boolean equals(Customer c2) { return this.email.equals(c2.getEmail()); }

    /* ***************** *
     * getter and setter *
     * ***************** */
    public String getName()  { return this.name;   }
    public String getEmail() { return this.email;  }
    public String getPhone() { return this.phone;  }

    public void setName(String n)  { name  = n; }
    public void setEmail(String s) { email = s; }
    public void setPhone(String s) { phone = s; }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean matches(String key) {
        String str = name + " " + email + " " + phone;
        str = str.replace("-", "");
        str = str.toLowerCase();
        String k = key.toLowerCase();
        return str.contains(k);
    }
}
