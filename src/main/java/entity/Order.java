package entity;

import struct.OrderDetail;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Created by adrianoob on 4/23/16.
 */

@XmlRootElement
public class Order {
    private int id = -1;
    private String status;
    @XmlTransient public final static String OPEN = "open";
    @XmlTransient public final static String DELIVERED = "delivered";
    @XmlTransient public final static String CANCELLED = "cancelled";
    @XmlTransient private static Menu menu;


    // purchase information
    // @XmlTransient private ArrayList<Item> item = new ArrayList();
    // @XmlTransient private ArrayList<Integer> quantity = new ArrayList();
    private OrderDetail [] order_detail;

    private double surcharge; // surcharge at the time of ordering. surcharge of the menu can be changed by manager.
    @XmlElement(name = "personal_info")
    private Customer customer;
    private String note;
    @XmlTransient private Date orderDate;
    private String order_Date;
    private double amount;

    // delivery information
    private String delivery_address;
    private String delivery_date;
    @XmlTransient private Date deliveryDate;

    private String holiday = "none";

    public Order() {
        status     = Order.OPEN;
        surcharge  = 0;
        deliveryDate = null;
        customer   = null;
        note       = null;
        order_Date = null;
        amount     = 0;
        delivery_address = null;
    }


    public static void setMenu(Menu menu) {
        Order.menu = menu;
    }

    /* ********************** *
     * basic order operations *
     * ********************** */

//    /**
//     * add an item to the shopping cart
//     * @param f      - item
//     * @param amount - quantity of the item
//     */
//    public void addItem(Item f, int amount) {
//        if (status == Order.OPEN) {
//            int index = indexOfItemInShoppingCart(f);
//            if (index < 0) {
//                item.add(f);
//                quantity.add(new Integer(amount));
//            } else {
//                Integer a = quantity.remove(index);
//                a += amount;
//                quantity.add(index, a);
//            }
//        }
//    }

//    /**
//     * remove an item from the shopping cart
//     * @param f - item to remove
//     */
//    public void removeItem(Item f) {
//        if (status == Order.OPEN) {
//            int index = indexOfItemInShoppingCart(f);
//            if (index >= 0) {
//                item.remove(index);
//                quantity.remove(index);
//            }
//        }
//    }

//    /**
//     * change the quantity of an item that is in the shopping cart
//     * @param f
//     * @param amount
//     */
//    public void changeItemQuantity(Item f, int amount) {
//        if (status == Order.OPEN) {
//            int index = indexOfItemInShoppingCart(f);
//            if (index >= 0) {
//                quantity.remove(index);
//                quantity.add(index, new Integer(amount));
//            }
//        }
//    }

    public boolean deliveryIsOnWeekendOrHoliday() {
        holiday = deliveryDate.getHoliday();
        return deliveryDate.isWeekend() || !holiday.equals("none");
    }

    public double totalPriceWithOutSurcharge() {
        double total = 0;
        for (int i = 0; i < order_detail.length; i++) {
            OrderDetail o = order_detail[i];
            Item f = menu.getItemWithIdOnAndOffMenu(o.getId());
            if (f.getId() > -1) {
                total += f.getPrice() * o.getCount();
            }
        }
        return total;
    }

    /*
     * look up the (exact) item Object in the shopping cart
     * and return its index. return -1 if not found
     */
//    private int indexOfItemInShoppingCart(Item f) {
//        int index = -1;
//        for (int i = 0; i < item.size(); i++) {
//            // WARNING: this logical equality should probably be reconsidered
//            if (item.get(i) == f) {
//                index = 0;
//                break;
//            }
//        }
//        return index;
//    }

    /* ***************** *
     * getter and setter *
     * ***************** */
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getDelivery_address() {
        return delivery_address;
    }

    public void setDelivery_address(String delivery_address) {
        this.delivery_address = delivery_address;
    }

    public String getOrder_date() {
        return orderDate.toString();
    }

    public void setOrder_date(String date) {
        orderDate = new Date(date);
    }

    public void setOrderDate(Date d) {
        orderDate = d;
    }

    public String getDelivery_date() {
        return deliveryDate.toString();
    }

    public void setDelivery_date(String date) {
        deliveryDate = new Date(date);
        if (deliveryDate.isWeekend())
            surcharge = menu.getSurcharge();
    }

//    public Item [] getItemList() {
//        int size = item.size();
//        Item [] list = new Item[size];
//        for (int i = 0; i < size; i++) {
//            list[i] = item.get(i);
//        }
//        return list;
//    }

//    public int [] getQuantityList() {
//        int size = quantity.size();
//        int [] list = new int[size];
//        for (int i = 0; i < size; i++) {
//            list[i] = quantity.get(i);
//        }
//        return list;
//    }

    public double getSurcharge() { return surcharge; }

    public void setSurcharge(double surcharge) {
        this.surcharge = surcharge;
    }

    public String getOrdered_by() { return customer.getEmail(); }

    public void setOrder_detail(OrderDetail [] detail) {
        order_detail = detail;
    }

    public OrderDetail [] getOrder_detail() {
        return order_detail;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getAmount() {
        return totalPriceWithOutSurcharge();
    }

    public void setAmount(double a) {}

    public String getHoliday() {
        new Holiday();
        holiday = Holiday.dateIsHoliday(deliveryDate);
        return holiday;
    }

    public void setHoliday(String holiday) {
        this.holiday = holiday;
    }
}
