package entity;

/**
 * Created by adrianoob on 4/26/16.
 */
public class BadItem extends Item {
    public BadItem() {
        super();
    }

    @Override
    public int getId() {
        return -1;
    }

    @Override
    public String getName() {
        return "item_does_not_exist";
    }

    @Override
    public double getPrice() {
        return -1;
    }

    @Override
    public int getMinimum() {
        return -1;
    }

    @Override
    public Category [] getCategory() {
        return new Category[]{};
    }
}
