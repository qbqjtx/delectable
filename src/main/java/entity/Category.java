package entity;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.ArrayList;
/**
 * Created by jj on 3/2/16.
 */
@XmlRootElement
public class Category {
    /* value for category when the category is missing or not set */
    public final static String DEFAULT = "unknown";

    @XmlTransient
    /* categoryPool contains the pre-defined non-repeat categories */
    private static ArrayList<String> categoryPool = null;

    private String name;

    /**
     * Default constructor
     * initialize categoryPool if it has not
     */
    public Category() {
        name = DEFAULT;
        if (categoryPool == null)
            categoryPool = new ArrayList<>();
    }

    public Category(String c) {
        if (categoryPool == null)
            categoryPool = new ArrayList<>();
        if (isInCategoryPool(c))
            name = c;
        else
            name = DEFAULT;
    }


    /**
     * Check whether a category is in categoryPool
     *
     * @param c - the category to be checked
     * @return boolean
     */
    private static boolean isInCategoryPool(String c) {
        boolean is = false;
        if (c != null && categoryPool.size() > 0 && !c.equals(DEFAULT)) {
            for (int i = 0; i < categoryPool.size(); i++) {
                if (categoryPool.get(i).equals(c)) {
                    is = true;
                    break;
                }
            }
        }
        return is;
    }

    /**
     * @return a string that represents all pre-defined categories from categoryPool
     */
    public static String getCategoryPoolAsString() {
        String result = "No Pre-defined Categories";
        int size = categoryPool.size();
        if (size > 0) {
            result = categoryPool.get(0);
            if (size > 1) {
                for (int i = 1; i < size; i++) {
                    result = result + ", " + categoryPool.get(i);
                }
            }
        }
        return result;
    }


    public static Category [] getCategoryPoolAsArray() {
        int size = categoryPool.size();
        Category [] re = new Category[size];
        for (int i = 0; i < size; i++) {
            re[i] = new Category(categoryPool.get(i));
        }
        return re;
    }


    /**
     * This method adds new pre-defined category to categoryPool
     *
     * @param c - a category to be added to pre-defined categoryPool
     */
    public static String addPreDefinedCategory(String c) {
        String feedback;
        if (c.equals(DEFAULT)) {
            feedback = DEFAULT + " is not allowed to be a category";
        } else if (!isInCategoryPool(c)) {
            categoryPool.add(c);
            feedback = c + " got added as a category";
        } else {
            feedback = c + " is already a category";
        }
        return feedback;
    }

    public static void removeAllCategory() {
        if (categoryPool != null)
            categoryPool.clear();
    }


    /* ************************ */
    /* ** getter and setter *** */
    /* ************************ */

    public String getName() {
        return name;
    }
    public void setName(String n) {
        if (isInCategoryPool(n))
            name = n;
        else
            name = DEFAULT;
    }

    public boolean equals(Category c2) {
        return this.name.equals(c2.name);
    }
}

