package entity;


import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.ArrayList;

/**
 * Created by adrianoob on 4/19/16.
 */

@XmlRootElement
public class Item {
    @XmlTransient private boolean hasHistory = false;
    @XmlTransient private String   t_name      = null;
    @XmlTransient private double   t_price     = -1;
    @XmlTransient private int      t_minimum   = -1;
    @XmlTransient private Category [] t_category  = {};

    private int id = -1;
    private String name;
    @XmlElement(name = "price_per_person")
    private double price = -1;
    @XmlElement(name = "minimum_order")
    private int minimum;
    @XmlElement(name = "categories")
    private Category [] category;

    @XmlTransient private Date create_date;
    @XmlTransient private Date last_modified_date;

    @XmlElement(name = "create_date")
    private String create_date_as_String = "";
    @XmlElement(name = "last_modified_date")
    private String last_modified_date_as_String = "";

    public Item() {
        this.name = "New_Food_Without_A_Name";
        this.price = -1;
        this.minimum = 0;
        this.category = new Category[0];
        create_date = new Date(); last_modified_date = create_date;
        create_date_as_String = create_date.toString();
        last_modified_date_as_String = last_modified_date.toString();
    }

    public Item(String n, double p, int m) {
        this.name = n;
        this.price = p;
        this.minimum = m;
        this.category = new Category[0];
        create_date = new Date(); last_modified_date = create_date;
        create_date_as_String = create_date.toString();
        last_modified_date_as_String = last_modified_date.toString();
    }

    public Item(String n, double p, int m, Category c) {
        this.name = n;
        this.price = p;
        this.minimum = m;
        Category [] cc = { c };
        setCategory(cc);
        create_date = new Date(); last_modified_date = create_date;
        create_date_as_String = create_date.toString();
        last_modified_date_as_String = last_modified_date.toString();
    }

    public Item(String n, double p, int m, Category [] c) {
        this.name = n;
        this.price = p;
        this.minimum = m;
        setCategory(c);
        create_date = new Date(); last_modified_date = create_date;
        create_date_as_String = create_date.toString();
        last_modified_date_as_String = last_modified_date.toString();
    }

    /* getters and setters */
    public void setName(String n)  {
        if (hasHistory)
            this.t_name = n;
        else
            this.name = n;
    }
    public void setPrice(double p) {
        if (hasHistory)
            this.t_price = p;
        else
            this.price = p;
    }
    public void setMinimum(int m)  {
        if (hasHistory)
            this.t_minimum = m;
        else
            this.minimum = m;
    }
    public void setHasHistory(boolean b) { this.hasHistory = b; }
    public void setId(int i) { id = id > -1 ? id : i; }
    public void setCategory(Category [] cl) {
        ArrayList<Category> temp = new ArrayList<>();
        for (Category c : cl) {
            if (!c.getName().equals(Category.DEFAULT))
                temp.add(c);
        }

        if (temp.size() == 0) {
            t_category = new Category[]{new Category()};
        } else {
            t_category = new Category[temp.size()];
            for (int i = 0; i < temp.size(); i++) {
                t_category[i] = temp.get(i);
            }
        }

        if (!hasHistory) {
            category = t_category;
            t_category = null;
        }
    }

    public String getName()    { return this.name; }
    public double getPrice()   { return this.price; }
    public int    getMinimum() { return this.minimum; }
    public int    getId()      { return this.id; }
    public Category [] getCategory() {
        return category;
    }

    /**
     * toString method
     * @return a string representation of the object
     */
    public String toString() {
        return String.format("setName:%s, setPrice:%.2f, setMinimum:%d, category:%s",
                this.getName(), this.getPrice(), this.getMinimum(), this.getCategoryAsString());
    }

    public String getCategoryAsString() {
        String r = Category.DEFAULT;
        if (category.length > 0)
            r = category[0].getName();
        if (category.length > 1) {
            for (int i = 1; i < category.length; i++) {
                r = r + ", " + category[i].getName();
            }
        }
        return r;
    }

    /**
     * check if the item belongs to a certain category, i.e. this.category contains that category
     * @param cat - category
     * @return    - boolean
     */
    public boolean belongsToCategory(Category cat) {
        boolean in = false;
        for (Category c : category) {
            if (c.equals(cat)) {
                in = true;
                break;
            }
        }
        return in;
    }

    /**
     * this method apply the changes that are planned to be made. If current object is a new object,
     * then change will be made to itself. If currently object has been used or has old version, then
     * the change will be made to a clone of this object, so that this object still exists and new
     * object will be returned
     * @return - an Item with changes applied to it.
     */
    public Item confirmChanges() {
        Item result;
        if (hasHistory) { // if current item has history, change will be made to a clone to keep history
            result = new Item();
        } else { // if current item has not history, change will be made to itself
            result = this;
        }

        result.setHasHistory(true); // now the new object is guaranteed to have history

        result.name  = t_name == null ? name : t_name;
        result.price = t_price == -1 ? price : t_price;
        result.minimum = t_minimum == -1 ? minimum : t_minimum;
        result.category = t_category == null ? category : t_category;

        // set t_values to default
        t_name = null;
        t_price = -1;
        t_minimum = -1;
        t_category = null;

        last_modified_date = new Date();
        return result;
    }

    public boolean equals(Item f2) {
        return id == f2.getId() && name.equals(f2.getName());
    }

    public void organizeCategory() {
        boolean hasDefault = false;
        for (Category c : category) {
            if (c.getName().equals(Category.DEFAULT)) {
                hasDefault = true;
                break;
            }
        }

        if (hasDefault && category.length > 1) {
            ArrayList<Category> temp = new ArrayList<>();
            for (Category c : category) {
                if (!c.getName().equals(Category.DEFAULT))
                    temp.add(c);
            }

            if (temp.size() == 0) {
                category = new Category[]{new Category()};
            } else {
                category = new Category[temp.size()];
                for (int i = 0; i < temp.size(); i++) {
                    category[i] = temp.get(i);
                }
            }
        } else if (category.length == 0) {
            category = new Category[]{new Category()};
        }
    }

    @XmlTransient
    public Date getLast_modified_date() {
        return last_modified_date;
    }

    @XmlTransient
    public void setLast_modified_date(Date last_modified_date) {
        this.last_modified_date = last_modified_date;
    }

    public String getCreate_date_as_String() {
        return create_date.toString();
    }
    public void setCreate_date_as_String(String d) {
        create_date = new Date(d);
    }

    public String getLast_modified_date_as_String() {
        return last_modified_date.toString();
    }
    public void setLast_modified_date_as_String(String d) {
        last_modified_date = new Date(d);
    }
}
