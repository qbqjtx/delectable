package entity;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Holiday Struct
 */
@XmlRootElement
public class Holiday {

    private String rule = "";
    private String description = "";
    @XmlTransient private static ArrayList<Holiday> holiday;

    public Holiday() {
        initializationLoadsDefaultHolidayList();
    }

    public Holiday(String r, String d) {
        rule = r;
        description = d;
        initializationLoadsDefaultHolidayList();
    }


    public static boolean addHoliday(String des, String rul) {
        boolean valid = validateRuleFormat(rul);
        if (valid)
            holiday.add(new Holiday(rul, des));
        return valid;
    }

    public static String dateIsHoliday(Date dat) {
        String holi = "";
        // **********************
        // **** exact date format
        // **********************
        String exact_format = String.format("%d/%d", dat.getMonth(), dat.getDay());

        // *********************************
        // **** format involving day of week
        // *********************************
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, dat.getYear());
        cal.set(Calendar.MONTH, dat.getMonth() -1);
        cal.set(Calendar.DATE, dat.getDay());
        int day_of_week_int = cal.get(Calendar.DAY_OF_WEEK);
        String date_of_week;
        switch (day_of_week_int) {
            case 1: date_of_week = "sunday"   ; break;
            case 2: date_of_week = "monday"   ; break;
            case 3: date_of_week = "tuesday"  ; break;
            case 4: date_of_week = "wednesday"; break;
            case 5: date_of_week = "thursday" ; break;
            case 6: date_of_week = "friday"   ; break;
            case 7: date_of_week = "saturday" ; break;
            default: date_of_week = "break;"  ; break;
        }
        int num_of_week = dat.getDay() / 7 + 1;
        cal.add(Calendar.DATE, 7);
        String week_format = String.format("%d/%d%s", dat.getMonth(), num_of_week, date_of_week);

        // ***********************
        // **** last monday format
        // ***********************
        boolean last = dat.getMonth() != cal.get(Calendar.MONTH);
        String week_format_last = String.format("%d/%d%s", dat.getMonth(), 5, date_of_week);

        for (int i = 0; i < holiday.size(); i++) {
            String rule = holiday.get(i).getRule();
            if (last) {
                if (rule.equals(exact_format) || rule.equals(week_format) || rule.equals(week_format_last)) {
                    holi = holiday.get(i).getDescription();
                    break;
                }
            } else {
                if (rule.equals(exact_format) || rule.equals(week_format)) {
                    holi = holiday.get(i).getDescription();
                    break;
                }
            }
        }
        return holi;
    }


    /* return true if format is
     * ** dd/dd
     * ** dd/dmonday
     */
    private static boolean validateRuleFormat(String rul) {
        boolean valid = true;
        int month = 0;
        int day = 0;
        if (rul == null) {
            valid = false;
        } else {
            String[] seg = rul.split("/");
            if (seg.length != 2) {
                valid = false;
            } else {
                month = intValue(seg[0]);
                if (month > 12 || month < 1) {
                    valid = false;
                } else {
                    day = intValue(seg[1]);
                    if (day < 1 || day > 31) {
                        if (day != -1) {
                            valid = false;
                        } else {
                            // now day = -1 means failing to parse to int
                            // possibly contains monday to sunday
                            String sub1 = seg[1].substring(0, 1);
                            int sub1int = intValue(sub1);
                            if (sub1int < 1 || sub1int > 5) {
                                valid = false;
                            } else {
                                String sub2 = seg[1].substring(1);
                                // first make sure sub2 doesn't have ","
                                // if it contains, it's not monday to sunday
                                // then see if "monday,tuesday,wednesday,thursday,friday,saturday,sunday" contains sub2
                                if (sub2.contains(",")) {
                                    valid = false;
                                } else {
                                    String special = "monday,Monday,tuesday,Tuesday,wednesday,Wednesday,thursday,Thursday,friday,Friday,saturday,Saturday,sunday,Sunday";
                                    if (!special.contains(sub2)) {
                                        valid = false;
                                    }
                                }
                            }
                        }
                    } else {
                        // day seems good,
                        // now check if such day exist
                        // pure dates
                        String mm   = String.format("%02d", intValue(seg[0]));
                        String dd   = String.format("%02d", intValue(seg[1]));
                        String date = "2001-" + mm + "-" + dd;
                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                        df.setLenient(false);
                        try {
                            df.parse(date);
                        } catch (ParseException pe) {
                            valid = false;
                        }
                    }
                }
            }
        }

        return valid;
    }


    /* loading a list of pre-defined federal holiday
     */
    private void initializationLoadsDefaultHolidayList() {
        if (holiday == null) {
            holiday = new ArrayList<>();
            holiday.add(new Holiday("1/1"      , "new_year"));
            holiday.add(new Holiday("1/3monday", "martin_luther_king_day"));
            holiday.add(new Holiday("5/5monday", "memorial_day"));
            holiday.add(new Holiday("7/4"      , "independence_day"));
            holiday.add(new Holiday("9/1monday", "labor_day"));
            holiday.add(new Holiday("11/24"    , "thanksgiving"));
            holiday.add(new Holiday("12/25"    , "christmas"));
        }
    }

    /* parse string to int
     *    ** return the int value if success
     *    ** return -1 otherwise
     */
    private static int intValue(String str) {
        int i = -1;
        try {
            i = Integer.parseInt(str);
        } catch (NumberFormatException nfe) {}
        return i;
    }

    public String getRule() {
        return rule;
    }

    public void setRule(String rule) {
        this.rule = rule;
    }

    public static Holiday [] getHolidayList() {
        return holiday.toArray(new Holiday[holiday.size()]);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
