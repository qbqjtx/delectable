package entity;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by adrianoob on 4/28/16.
 */
@XmlRootElement
public class Report {
    private int id;
    private String name;
    private int number_of_orders;
    private Order [] orders;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Order[] getOrders() {
        return orders;
    }

    public void setOrders(Order[] order) {
        this.orders = order;
    }

    public Report(){
        id = 0;
        name = "unknown report";
        orders = new Order[0];
    }

    public Report(int i, String n, Order [] o){
        id = i;
        name = n;
        orders = o;
    }

    public int getNumber_of_orders() {
        Order [] o = getOrders();
        if (o != null && o.length > 0)
            number_of_orders = o.length;
        return number_of_orders;
    }

    public void setNumber_of_orders(int num) {
        Order [] o = getOrders();
        if (o != null && o.length > 0)
            number_of_orders = o.length;
    }

}
