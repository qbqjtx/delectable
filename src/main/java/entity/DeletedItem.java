package entity;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by adrianoob on 4/26/16.
 */
@XmlRootElement
public class DeletedItem extends Item {
    public DeletedItem() {
        super();
    }

    @Override
    public int getId() {
        return -1;
    }

    @Override
    public String getName() {
        return "item_is_removed";
    }

    @Override
    public double getPrice() {
        return -1;
    }

    @Override
    public int getMinimum() {
        return -1;
    }

    @Override
    public Category [] getCategory() {
        return new Category[]{};
    }
}
