package struct;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by adrianoob on 4/26/16.
 */
@XmlRootElement
public class OrderDetail {
    private int id;
    private int count;

    public OrderDetail(){}

    public OrderDetail(int i, int c) {
        id = i;
        count = c;
    }

    public void add(int c) {
        count += c;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
