package struct;

import entity.Customer;
import entity.Order;


import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by adrianoob on 4/28/16.
 */
@XmlRootElement
public class Customer_Orders {
    private Customer customer;
    private Order[] order;


    public Customer_Orders() {}
    public Customer_Orders(Customer cus, Order [] ord) {
        customer = cus;
        order = ord;
    }

    public Order[] getOrder() {
        return order;
    }

    public void setOrder(Order[] order) {
        this.order = order;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
}
