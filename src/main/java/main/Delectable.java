package main;

import entity.*;

import java.util.ArrayList;

/**
 * Created by adrianoob on 4/24/16.
 */
public class Delectable {
    private static int totalOrder = 0;
    private static int totalCustomer = 0;
    private static Menu menu = null;
    private static ArrayList<Order> orderList = null;
    private static ArrayList<Customer> customerList = null;
    private static Order cart = null;
    private static Holiday holiday = null;

    private static Delectable d = new Delectable();

    private Delectable() {
        if (holiday == null) holiday = new Holiday();
        if (menu == null) menu = new Menu();
        if (orderList == null) orderList = new ArrayList<>();
        if (customerList == null) customerList = new ArrayList<>();
        Delectable.cart = new Order();
        Delectable.cart.setMenu(menu);
    }


    /* ---------------------------------------- *
    /* ---------------------------------------- *
     * -------------- Holiday ----------------- *
     * ---------------------------------------- *
     * ---------------------------------------- */
    public static Holiday [] getHolidaylist() {
        return Holiday.getHolidayList();
    }

    public static boolean addHoliday(String des, String rul) {
        return Holiday.addHoliday(des, rul);
    }


    /* ---------------------------------------- *
    /* ---------------------------------------- *
     * ---------------- Menu ------------------ *
     * ---------------------------------------- *
     * ---------------------------------------- */

    /** Manager / Owner
     * define a menu category. category need to be defined by this method before using.
     * @param cat - category to define
     */
    public static String defineNewMenuCategory(String cat) {
        return menu.defineNewCategory(cat);
    }

    /** Manager / Owner
     * add an item to menu
     * @param f - item to add
     */
    public static void addItemToMenu(Item f) {
        menu.addItemToMenu(f);
    }

    /**
     * get the item with specified id
     * @param id - id of the item
     * @return   - the item
     */
    public static Item getItemWithId(int id) {
        return menu.getItemWithIdOnMenu(id);
    }

    /** Manager / Owner
     * delete an item from menu
     * @param id - setId of the item to delete
     */
    public static void deleteItemFromMenu(int id) {
        menu.deleteItemFromMenu(id);
    }

    /** Manager / Owner
     * change setPrice of an item ON menu via its ID
     * @param id    - item setId
     * @param price - new setPrice
     */
    public static int changeItemPrice(int id, double price) {
        return menu.changeItemPrice(id, price);
    }

    /** Everyone
     * browse all available pre-defined categories
     * @return - all categories
     */
    public static Category [] getAllItemCategories() {
        return menu.definedCategory();
    }

    /** Everyone
     * browse the entire menu
     * @return - all the items
     */
    public static Item [] getAllTheItemsFromMenu() {
        return menu.getWholeMenu();
    }

    /** Everyone
     * browse items from one category
     * @param cat - category
     * @return    - items from the category
     */
    public static Item [] getItemsFromCategory(Category cat) {
        return menu.getItemFromCategory(cat);
    }

    /**
     * get the surcharge
     * @return - surcharge
     */
    public static double getSurcharge() {
        return menu.getSurcharge();
    }

    /**
     * set new surcharge
     * @param rate - new surcharge
     */
    public static void setSurcharge(double rate) {
        menu.setSurcharge(rate);
    }

    /* ---------------------------------------- *
     * ---------------------------------------- *
     * ---------------- Order ----------------- *
     * --------- Delivery Information --------- *
     * ---------------------------------------- *
     * ---------------------------------------- */

    public void setDeliveryDate(String date) {
        cart.setDelivery_date(date);
    }

    public void setCustomer(Customer c) {
        cart.setCustomer(c);
    }

    public void setDeliveryAddress(String a) {
        cart.setDelivery_address(a);
    }

    public void setSpecialInstruction(String si) {
        cart.setNote(si);
    }

    /* ---------------------------------------- *
     * ---------------- Order ----------------- *
     * ---------- System Operations ----------- *
     * -------- methods to be used when ------- *
     * ---- cart information is complete ----- *
     * ---------------------------------------- */

    /**
     * check if delivery is on Weekend
     * @return - boolean to indicate whether delivery is on weekend
     */
    public boolean deliveryIsOnWeekend() {
        boolean is = cart.deliveryIsOnWeekendOrHoliday();
        if (is) cart.setSurcharge(menu.getSurcharge());
        return is;
    }

    /**
     * calculate total setPrice in shopping cart without surcharge
     * @return - total without surcharge
     */
    public double totalWithOutSurcharge() {
        return cart.totalPriceWithOutSurcharge();
    }

    /**
     * submit order and return order id
     * @param ord - the order
     * @return    - order id
     */
    public static int submitOrder(Order ord) {
        orderList.add(ord);
        ord.setId(totalOrder);
        Date today = new Date();
        ord.setOrderDate(today);
        Customer sameCustomInRecord = recordCustomer_ifExistsThenReturnIt(ord.getCustomer());
        if (sameCustomInRecord != null)
            ord.setCustomer(sameCustomInRecord);
        return totalOrder++;
    }

    /* ----------------------------------------- *
     * ------------- Order search -------------- *
     * ----------------------------------------- *
     * --------- by date & by customer --------- *
     * ----------------------------------------- */


    public static ArrayList<Order> getAllOrders() {
        return orderList;
    }

    public static Order getOrderById(int id) {
        Order ord = new MissingOrder();
        if (id >= 0 && id < orderList.size())
            ord = orderList.get(id);
        return ord;
    }

    /**
     * get today's orders
     * @return - array containing today's orders
     */
    public static Order [] getOrderOfToday() {
        Date today = new Date();
        ArrayList<Order> temp = new ArrayList<>();
        for (int i = 0; i < orderList.size(); i++) {
            if (orderList.get(i).getDelivery_date().equals(today.toString()))
                temp.add(orderList.get(i));
        }

        Order [] list = new Order[temp.size()];
        for (int i = 0; i < temp.size(); i++) {
            list[i] = temp.get(i);
        }
        return list;
    }

    /**
     * get tomorrow's orders
     * @return - array containing tomorrow's orders
     */
    public static Order [] getOrderOfTomorrow() {
        Date today = new Date();
        Date tomorrow = today.roll(1);
        ArrayList<Order> temp = new ArrayList<>();
        for (int i = 0; i < orderList.size(); i++) {
            if (orderList.get(i).getDelivery_date().equals(tomorrow.toString()))
                temp.add(orderList.get(i));
        }

        Order [] list = new Order[temp.size()];
        for (int i = 0; i < temp.size(); i++) {
            list[i] = temp.get(i);
        }

        return list;
    }

    private static Customer recordCustomer_ifExistsThenReturnIt(Customer cus) {
        boolean in = false;
        Customer cuss = null;
        for (int i = 0; i < customerList.size(); i++) {
            if (customerList.get(i).equals(cus)) {
                in = true;
                cuss = customerList.get(i);
                break;
            }
        }

        if (!in) {
            cus.setId(totalCustomer++);
            customerList.add(cus);
        }

        return cuss;
    }

    public static ArrayList<Customer> getAllCustomers() {
        return customerList;
    }

    public static Customer getCustomerById(int cid) {
        Customer cus = new MissingCustomer();
        if (cid >= 0 && cid < customerList.size())
            cus = customerList.get(cid);
        return cus;
    }

    public static ArrayList<Customer> searchCustomers(String key) {
        ArrayList<Customer> list = new ArrayList<>();
        for (int i = 0; i < customerList.size(); i++) {
            if (customerList.get(i).matches(key))
                list.add(customerList.get(i));
        }
        return list;
    }

    public static Order [] searchOrderByCustomer(Customer cos) {
        ArrayList<Order> temp = new ArrayList<>();
        for (int i = 0; i < orderList.size(); i++) {
            if (orderList.get(i).getCustomer() == cos)
                temp.add(orderList.get(i));
        }

        Order [] list = new Order[temp.size()];
        for (int i = 0; i < temp.size(); i++) {
            list[i] = temp.get(i);
        }
        return list;
    }

    /* ---------------------------------------- *
     * ---------------------------------------- *
     * ---------- statistic report ------------ *
     * ---------------------------------------- *
     * ---------------------------------------- */

    public static ArrayList<Order> allOrdersInTheTimeInterval(Date d1, Date d2) {
        ArrayList<Order> list = new ArrayList<>();
        for (int i = 0; i < orderList.size(); i++) {
            Order o = orderList.get(i);
            Date d = new Date(o.getDelivery_date());
            if (d1 == null && d2 == null) {
                list.add(o);
            }
            else if (d1 == null && d2 != null) {
                if ((d.isBefore(d2) || d.equals(d2)))
                    list.add(o);
            } else if (d1 != null && d2 == null) {
                if((d.isAfter(d1) || d.equals(d1)))
                    list.add(o);
            } else {
                if ((d.isAfter(d1) || d.equals(d1)) && (d.isBefore(d2) || d.equals(d2)))
                    list.add(o);
            }
        }
        return list;
    }

    public static ArrayList<Order> ordersToDeliverOnDate(Date date) {
        ArrayList<Order> list = new ArrayList<>();
        for (int i = 0; i < orderList.size(); i++) {
            Order o = orderList.get(i);
            Date d = new Date(o.getDelivery_date());
            if (d.equals(date) && o.getStatus().equals((Order.OPEN)))
                list.add(o);
        }
        return list;
    }



//    /**
//     * @return - number of delivered orders this month (from 1st of the month)
//     */
//    public int numberOfOrdersDeliveredThisMonth() {
//        Date d2 = new Date();
//        Date d1 = new Date(d2.getYear(), d2.getMonth(), 1, 1, 1);
//        Order [] list = ordersInTheTimeInterval(d1, d2);
//        return list.length;
//    }

//    /**
//     * @return - revenue of delivered orders this month (from 1st of the month)
//     */
//    public double revenueOfThisMonth() {
//        Date d2 = new Date();
//        Date d1 = new Date(d2.getYear(), d2.getMonth(), 1, 1, 1);
//        Order [] list = ordersInTheTimeInterval(d1, d2);
//        double revenue = 0;
//        for (Order ord : list) {
//            revenue += ord.totalPriceWithOutSurcharge() + ord.getSurcharge();
//        }
//        return revenue;
//    }
//
//    /**
//     * @return - number of orders delivered last month
//     */
//    public int numberOfOrdersDeliveredLastMonth() {
//        Date today = new Date();
//        Date first_day_of_this_month = new Date(today.getYear(), today.getMonth(), 1, 1, 1);
//        Date d2 = first_day_of_this_month.roll(-1);
//        Date d1 = new Date(d2.getYear(), d2.getMonth(), 1, 1, 1);
//        Order [] list = ordersInTheTimeInterval(d1, d2);
//        return list.length;
//    }
//
//    /**
//     * @return - revenue made from delivered orders in the last month
//     */
//    public double revenueOfLastMonth() {
//        Date today = new Date();
//        Date first_day_of_this_month = new Date(today.getYear(), today.getMonth(), 1, 1, 1);
//        Date d2 = first_day_of_this_month.roll(-1);
//        Date d1 = new Date(d2.getYear(), d2.getMonth(), 1, 1, 1);
//        Order [] list = ordersInTheTimeInterval(d1, d2);
//        double revenue = 0;
//        for (Order ord : list) {
//            revenue += ord.totalPriceWithOutSurcharge() * ord.getSurcharge();
//        }
//        return revenue;
//    }

//    /**
//     * @param n - number of months. a month is 30 days.
//     * @return  - number of orders delivered in the last n months
//     */
//    public int numberOfOrdersDeliveredOverMonths(int n) {
//        Date d2 = new Date();
//        Date d1 = d2.roll(- 30 * n);
//        Order [] list = ordersInTheTimeInterval(d1, d2);
//        return list.length;
//    }
//
//
//    public double revenuesOverMonths(int n) {
//        Date d2 = new Date();
//        Date d1 = d2.roll(- 30 * n);
//        Order [] list = ordersInTheTimeInterval(d1, d2);
//        double revenue = 0;
//        for (Order ord : list) {
//            revenue += ord.totalPriceWithOutSurcharge() * ord.getSurcharge();
//        }
//        return revenue;
//    }

}
