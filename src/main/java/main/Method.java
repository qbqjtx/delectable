package main;

/**
 * Created by adrianoob on 4/25/16.
 */
public class Method {
    public static String arrayToString(String [] l) {
        String ret = "{";
        int n = l.length / 2;
        for (int i = 0; i < n; i++) {
            String key   = "\"" + l[2 * i] + "\"";
            String value = l[2 * i + 1];
            ret = ret + key + ":" + value;
            if (i < n-1)
                ret = ret + ",";
        }
        ret = ret + "}";
        return ret;
    }

    public static String notFound(String message) {
        return "<!DOCTYPE html>\n" +
                "<html>\n" +
                "    <head>\n" +
                "        <title>error report</title>\n" +
                "        <style type='text/css'>\n" +
                "        H1 {font-family:\"Palatino Linotype\",\"Book Antiqua\",Palatino,serif;color:grey;font-size:28px;} \n" +
                "        BODY {font-family:\"Palatino Linotype\",\"Book Antiqua\",Palatino,serif;color:grey;margin:20px auto; width: 400px;} \n" +
                "        B {font-family:\"Palatino Linotype\",\"Book Antiqua\",Palatino,serif;color:grey;}\n" +
                "        P {font-family:\"Palatino Linotype\",\"Book Antiqua\",Palatino,serif;color:black;font-size:16px;}\n" +
                "        th, td {padding: 15px;}\n" +
                "        </style> \n" +
                "    </head>\n" +
                "    <body>\n" +
                "        <h1>HTTP Status 404 - Not Found</h1>\n" +
                "        <table style='width:100%'>\n" +
                "            <col width='20%'>\n" +
                "            <col width='80%'>\n" +
                "            <tr>\n" +
                "                <td><i><b>message</b></i></td> \n" +
                "                <td>"+ message +"</td>\n" +
                "            </tr>\n" +
                "        </table>\n" +
                "    </body>\n" +
                "</html>";
    }

    public static String badRequest(String message) {
        return "<!DOCTYPE html>\n" +
                "<html>\n" +
                "    <head>\n" +
                "        <title>error report</title>\n" +
                "        <style type='text/css'>\n" +
                "        H1 {font-family:\"Palatino Linotype\",\"Book Antiqua\",Palatino,serif;color:grey;font-size:28px;} \n" +
                "        BODY {font-family:\"Palatino Linotype\",\"Book Antiqua\",Palatino,serif;color:grey;margin:20px auto; width: 400px;} \n" +
                "        B {font-family:\"Palatino Linotype\",\"Book Antiqua\",Palatino,serif;color:grey;}\n" +
                "        P {font-family:\"Palatino Linotype\",\"Book Antiqua\",Palatino,serif;color:black;font-size:16px;}\n" +
                "        th, td {padding: 15px;}\n" +
                "        </style> \n" +
                "    </head>\n" +
                "    <body>\n" +
                "        <h1>HTTP Status 400 - Bad Request</h1>\n" +
                "        <table style='width:100%'>\n" +
                "            <col width='20%'>\n" +
                "            <col width='80%'>\n" +
                "            <tr>\n" +
                "                <td><i><b>message</b></i></td> \n" +
                "                <td>"+ message +"</td>\n" +
                "            </tr>\n" +
                "        </table>\n" +
                "    </body>\n" +
                "</html>";
    }

}
