package test;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

import entity.Item;
import entity.Category;
/**
 * Created by adrianoob on 4/19/16.
 */
public class ItemTest {
    Item f;
    Category [] meat;

    @Before
    public void setUp() {
        new Category();
        Category.removeAllCategory();
        Category.addPreDefinedCategory("Meat");
        Category.addPreDefinedCategory("Vegan");
        Category.addPreDefinedCategory("Seafood");

        f = new Item();

        meat = new Category[1]; meat[0] = new Category("Meat");
    }

    @Test
    public void _createAnItem() {
        assertEquals("default constructor test", "setName:New_Food_Without_A_Name, setPrice:0.00, setMinimum:0, category:unknown", f.toString());
        f = new Item("N", 1, 2);
        assertEquals("non-default constructor test", "setName:N, setPrice:1.00, setMinimum:2, category:unknown", f.toString());

    }

    @Test
    public void _confirmChangeWithDefaultConstructor() {
        Category [] c = new Category[2]; c[0] = new Category("Meat"); c[1] = new Category("fancy");
        f.setName("Steak"); f.setPrice(10); f.setMinimum(1); f.setCategory(c);
        Item f0 = f.confirmChanges();
        assertEquals("confirm change to new food", "setName:Steak, setPrice:10.00, setMinimum:1, category:Meat", f.toString());

        c = new Category[3]; c[0] = new Category("Meat"); c[1] = new Category("fancy"); c[2] = new Category("Vegan");
        f.setCategory(c);
        Item g = f.confirmChanges();
        assertEquals("further change won't affect old item", "setName:Steak, setPrice:10.00, setMinimum:1, category:Meat", f.toString());
        assertEquals("old item is the same item", true, f0 == f);
        assertEquals("further change will apply to new item", "setName:Steak, setPrice:10.00, setMinimum:1, category:Meat, Vegan", g.toString());

        g.setPrice(11);
        g.setMinimum(2);
        g.setName("Tomato");
        Item i = g.confirmChanges();
        assertEquals("other attributes can be changed correctly", "setName:Tomato, setPrice:11.00, setMinimum:2, category:Meat, Vegan", i.toString());
    }

    public void _belongsToCategory() {
        Category [] cs = new Category[2];
        cs[0] = new Category("Meat");
        cs[1] = new Category("Seafood");
        f.setCategory(cs);

        assertEquals("item belongs to Meat", true, f.belongsToCategory(new Category("Meat")));
        assertEquals("item belongs to Seafood", true, f.belongsToCategory(new Category("Seafood")));


    }
}