package test;

import entity.Category;
import org.junit.Before;
import entity.Item;
import entity.Menu;

/**
 * Created by adrianoob on 4/23/16.
 */
public class OrderTest {
    @Before
    public void setUp() {
        final String C1 = "STARTERS";
        final String C2 = "SOUP & SALADS";
        final String C3 = "DINNER ENTR\u00c9ES";
        Menu menu = new Menu();
        Category c1, c2, c3;

        // 1. prepare categories
        menu.defineNewCategory(C1);
        menu.defineNewCategory(C2);
        menu.defineNewCategory(C3);
        c1 = new Category(C1); c2 = new Category(C2); c3 = new Category(C3);


        // 2 & 3. prepare food items and menu
        // starters
        menu.addItemToMenu(new Item("Pommes Frites", 5, 6, c1));
        menu.addItemToMenu(new Item("Risotto Balls", 9.5, 6, c1));
        menu.addItemToMenu(new Item("Chicken Wings", 6, 4, c1));

        // salad and soup
        menu.addItemToMenu(new Item("Peasant Soup",5.5, 5, c2));
        menu.addItemToMenu(new Item("Grilled Caesar Salad", 5.5, 10, c2));
        menu.addItemToMenu(new Item("House Salad", 6, 8, c2));

        //dinner entrees
        menu.addItemToMenu(new Item("Sweet Potato Gnocchi", 17.5, 2, c3));
        menu.addItemToMenu(new Item("Ozark Meat Pie", 18, 2, c3));
    }

    // test addItem()

    // test removeItem()

    // test changeItemQuantity()

    // test deliveryIsOnWeekendOrHoliday()


}