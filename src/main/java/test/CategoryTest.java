package test;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

import entity.Category;

/**
 * Created by jj on 3/2/16.
 */
public class CategoryTest {
    private static final String D = Category.DEFAULT;
    Category cate = null;
    @Before
    public void setUp() {
        cate = new Category();
        Category.removeAllCategory();
    }

    @Test
    public void _testConstructor() {
        assertEquals("test constructor 1", "No Pre-defined Categories", Category.getCategoryPoolAsString());
        assertEquals("test constructor 2", D, cate.getName());
    }

    @Test
    public void _testPreDefinedCategoryPool() {
        Category.addPreDefinedCategory("Meat");
        assertEquals("add 1st pre-defined category", "Meat", Category.getCategoryPoolAsString());

        Category.addPreDefinedCategory("Vegan");
        assertEquals("add 2nd pre-defined category", "Meat, Vegan", Category.getCategoryPoolAsString());

        Category.addPreDefinedCategory("Meat");
        assertEquals("add repeated pre-defined category", "Meat, Vegan", Category.getCategoryPoolAsString());

        Category.removeAllCategory();
        assertEquals("remove all category", "No Pre-defined Categories", Category.getCategoryPoolAsString());
    }

    @Test
    public void _testCategoryList() {
        Category.addPreDefinedCategory("Meat");
        Category.addPreDefinedCategory("Vegan");

        // test method addToCategoryList
        cate.setName("Meat");
        assertEquals("setter success 1", "Meat", cate.getName());
        cate.setName("Vegan");
        assertEquals("setter success 2", "Vegan", cate.getName());
        cate.setName("random");
        assertEquals("setter fail 1", D, cate.getName());
        cate.setName(null);
        assertEquals("setter fail 2", D, cate.getName());
    }

    @Test
    public void _testGetCategoryPoolAsArray() {
        assertEquals("initially array has length 0", 0, Category.getCategoryPoolAsArray().length);

        Category.addPreDefinedCategory("Meat");
        Category.addPreDefinedCategory("Vegan");
        Category [] a = Category.getCategoryPoolAsArray();
        System.out.println(a[0].getName());
        System.out.println(a[1].getName());

        assertEquals("array returned is correct 1", "Meat", a[0].getName());
        assertEquals("array returned is correct 2", "Vegan", a[1].getName());

    }

}