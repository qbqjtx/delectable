package test;

import main.Method;
import org.junit.Test;
import struct.OrderDetail;

import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Created by adrianoob on 4/26/16.
 */
public class MethodTest {

    @Test
    public void _testArrayToString() {
        String [] s = {"id", "1", "count", "5"};
        assertEquals("case with numeric values", "{\"id\":1,\"count\":5}", Method.arrayToString(s));

        s = new String[]{"id", "1", "count", "4", "status", "\"wicked\""};
        assertEquals("case with String valeus","{\"id\":1,\"count\":4,\"status\":\"wicked\"}", Method.arrayToString(s));
    }

    @Test
    public void _experiments() {
        ArrayList<OrderDetail> od = new ArrayList<>();
        od.add(new OrderDetail(1,1));
        od.add(new OrderDetail(2,2));
        od.add(new OrderDetail(3,3));

        OrderDetail [] list = od.toArray(new OrderDetail[od.size()]);
        for (OrderDetail oo : list) {
            System.out.println(oo.getId() + oo.getCount());
        }
    }

}