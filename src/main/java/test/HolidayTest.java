package test;

import entity.Date;
import entity.Holiday;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by adrianoob on 4/29/16.
 */
public class HolidayTest {
    @Test
    public void _test() {
        new Holiday();
        assertEquals(true, Holiday.dateIsHoliday(new Date("20160118"))); // martin luther king
        assertEquals(false, Holiday.dateIsHoliday(new Date("20150118")));
        assertEquals(true, Holiday.dateIsHoliday(new Date("20160530"))); // memorial day
        assertEquals(false, Holiday.dateIsHoliday(new Date("20170530")));
        assertEquals(true, Holiday.dateIsHoliday(new Date("20170529"))); // memorial day
        assertEquals(true, Holiday.dateIsHoliday(new Date("20161124"))); // thanksgiving
        assertEquals(true, Holiday.dateIsHoliday(new Date("20161225"))); // christmas
    }
}