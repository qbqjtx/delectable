package test;

import org.junit.Test;
import entity.Date;

import static org.junit.Assert.*;

/**
 * Created by adrianoob on 4/24/16.
 */
public class DateTest {
    @Test
    public void _isWeekend() {
        Date d2 = new Date(2016, 5, 6, 12, 0);
        assertEquals("can tell weekday(Friday)", false, d2.isWeekend());
        Date d3 = new Date(2016, 5, 7, 12, 0);
        assertEquals("can tell weekend(Saturday)", true, d3.isWeekend());
        Date d4 = new Date(2016, 5, 8, 12, 0);
        assertEquals("can tell weekend(Sunday)", true, d4.isWeekend());
        Date d5 = new Date(2016, 5, 9, 12, 0);
        assertEquals("can tell weekday(Monday)", false, d5.isWeekend());
    }

    @Test
    public void _equals() {
        Date d1 = new Date(2016, 3, 2, 0, 0);
        Date d2 = new Date(2016, 3, 2, 1, 1);
        assertEquals("Date equals", true, d1.equals(d2));
    }

    @Test
    public void _roll() {
        Date d0 = new Date(2016, 5, 1, 2, 3);
        Date temp = new Date(2016, 5, 11, 2, 3);
        Date d = d0.roll(10);
        assertEquals("roll: year correct", 2016, d.getYear());
        assertEquals("roll: month correct", 5, d.getMonth());
        assertEquals("roll: day correct", 11, d.getDay());
        assertEquals("roll: isWeekend() correct", false, d.isWeekend());
        assertEquals("roll: correct as whole", true, d.equals(temp));


        Date dd = d.roll(-20);
        temp = new Date(2016, 4, 21, 1, 10);
        assertEquals("roll back correct", true, dd.equals(temp));
    }

    @Test
    public void _isAfterAndBefore() {
        Date d1 = new Date(2016, 5, 1, 12, 12);
        Date d2 = new Date(2016, 5, 1, 12, 11);
        Date d3 = new Date(2016, 5, 2, 0, 0);
        assertEquals("compare date case 1", true, d1.isAfter(d2));
        assertEquals("compare date case 2", false, d2.isAfter(d1));
        assertEquals("compare date case 3", true, d3.isAfter(d2));

        assertEquals("compare date case 1", true, d2.isBefore(d1));
        assertEquals("compare date case 2", false, d3.isBefore(d2));
        assertEquals("compare date case 3", true, d2.isBefore(d3));
    }

    @Test
    public void _constructorWithStringArgument() {
        Date d1 = new Date(2016, 5, 1, 12, 12);
        Date d2 = new Date("20160501");
        assertEquals("constructor that takes String should work", true, d1.equals(d2));
    }

}