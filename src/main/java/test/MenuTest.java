package test;

import entity.Category;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

import entity.Menu;
import entity.Item;

/**
 * Created by adrianoob on 4/22/16.
 */
public class MenuTest {
    final String C1 = "STARTERS";
    final String C2 = "SOUP & SALADS";
    final String C3 = "DINNER ENTR\u00c9ES";

    Category c1, c2, c3;

    Menu menu;
    @Before
    public void setUp() {
        // To use a menu:
        // 1. define categories
        // 2. create food / drink items with defined categories
        // 3. add them to menu

        Category.removeAllCategory(); // make sure not be affected by categories defined in other tests

        menu = new Menu();
        // 1. prepare categories
        menu.defineNewCategory(C1);
        menu.defineNewCategory(C2);
        menu.defineNewCategory(C3);

        c1 = new Category(C1); c2 = new Category(C2); c3 = new Category(C3);


        // 2 & 3. prepare food items and menu
        // starters
        menu.addItemToMenu(new Item("Pommes Frites", 5, 6, c1));
        menu.addItemToMenu(new Item("Risotto Balls", 9.5, 6, c1));
        menu.addItemToMenu(new Item("Chicken Wings", 6, 4, c1));

        // salad and soup
        menu.addItemToMenu(new Item("Peasant Soup",5.5, 5, c2));
        menu.addItemToMenu(new Item("Grilled Caesar Salad", 5.5, 10, c2));
        menu.addItemToMenu(new Item("House Salad", 6, 8, c2));

        //dinner entrees
        menu.addItemToMenu(new Item("Sweet Potato Gnocchi", 17.5, 2, c3));
        menu.addItemToMenu(new Item("Ozark Meat Pie", 18, 2, c3));
    }

    @Test
    public void _getItemFromCategory() {
        // create some items have more than one category
        Category [] strange_categories_1 = {c1, c2};
        menu.addItemToMenu(new Item("Strange Food1", 10, 1, strange_categories_1));
        Category [] strange_categories_2 = {c1, c3};
        menu.addItemToMenu(new Item("Strange Food2", 10, 2, strange_categories_2));
        Category [] strange_categories_3 = {c1, c2, c3};
        menu.addItemToMenu(new Item("Strange Food3", 10, 3, strange_categories_3));

        Item [] fc1 = menu.getItemFromCategory(c1);
        assertEquals("C1 has item 0", "Pommes Frites", fc1[0].getName());
        assertEquals("C1 has item 1", "Risotto Balls", fc1[1].getName());
        assertEquals("C1 has item 2", "Chicken Wings", fc1[2].getName());
        assertEquals("C1 has item 3", "Strange Food1", fc1[3].getName());
        assertEquals("C1 has item 4", "Strange Food2", fc1[4].getName());
        assertEquals("C1 has item 5", "Strange Food3", fc1[5].getName());

        Item [] fc2 = menu.getItemFromCategory(c2);
        assertEquals("C2 has item 0", "Peasant Soup", fc2[0].getName());
        assertEquals("C2 has item 1", "Grilled Caesar Salad", fc2[1].getName());
        assertEquals("C2 has item 2", "House Salad", fc2[2].getName());
        assertEquals("C2 has item 3", "Strange Food1", fc2[3].getName());
        assertEquals("C2 has item 4", "Strange Food3", fc2[4].getName());

        Item [] fc3 = menu.getItemFromCategory(c3);
        assertEquals("C3 has item 0", "Sweet Potato Gnocchi", fc3[0].getName());
        assertEquals("C3 has item 1", "Ozark Meat Pie", fc3[1].getName());
        assertEquals("C3 has item 2", "Strange Food2", fc3[2].getName());
        assertEquals("C3 has item 3", "Strange Food3", fc3[3].getName());
    }


    @Test
    public void _changeItemPrice() {
        menu.changeItemPrice(3, 7);
        Item [] all = menu.getWholeMenu();
        assertEquals("list move up", 4, all[3].getId());
        assertEquals("new setPrice is on effect and object is at end of the array", 7, all[7].getPrice(), 0.01);
        assertEquals("old item is in deletedList", "Peasant Soup", menu.getWholeDeletedMenu()[0].getName());
    }
}