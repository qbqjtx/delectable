package test;

import org.junit.Before;
import org.junit.Test;
import entity.Customer;


import static org.junit.Assert.*;

/**
 * Created by adrianoob on 4/23/16.
 */
public class CustomerTest {
    Customer cos1;
    Customer cos2;
    Customer cos3;
    @Before
    public void setUp() {
        cos1 = new Customer("name1", "example@address", "12212212122");
        cos2 = new Customer("name2", "example@address", "13313313133");
        cos3 = new Customer("name3", "example@address2", "13313313133");
    }

    @Test
    public void _testEqual() {
        assertEquals("equals should exam email", true, cos1.equals(cos2));
        assertEquals("equals should not exam others", false, cos3.equals(cos2));
    }

    @Test
    public void _testMatches() {
        cos1 = new Customer("Firstname Lastname", "example@address", "122-12212122");

        assertEquals("matches name", true, cos1.matches("firstName"));
        assertEquals("matches phone (partial)", true, cos1.matches("122122"));

    }
}